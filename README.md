#JUnit Test - Assignment 1-2
---

##Student Names

* Deivid Mafra ------------			 Student ID: C0752173
* Sherwayne Palmer ------			 Student ID: C0738019
* Diego Perez -------------			 Student ID: C0746800

---

##Report Link

https://docs.google.com/spreadsheets/d/11dL27VXJDyz_GWcHEUeccrJQEdHiTtJ9Q-WQZEEqkcU/edit?usp=sharing

---

##Description

In this activity, we are practice using OOP and JUnit to perform unit testing on a simple Banking application.

---

