import static org.junit.Assert.*;

import org.junit.Test;

public class TestBankCases {

	/**
	 * B1: When a new Bank is created, it has 0 customers.*/
	@Test 
	public void testCreateBank() {
		//1. Create a bank
		Bank bank = new Bank();
		
		//2. Get number of customers
		int numberOfCustomers = bank.getNumberOfCustomers();
		
		//3. Assert
		assertEquals(0,numberOfCustomers);
	}
	
	/**
	*B2: The bank can add customers. When a customer is added,The number of Bank customers increases by 1*/
	@Test 
	public void testAddCustToBank() {
		//1. Create a bank
		Bank bank = new Bank();
		
		//2. initialize current number of customers
		int currentNumOfCustomers = 0;
		
		//3. Add customer to bank
		bank.addCustomer("Sherwayne", 1000); //number of customer(s) increased by 1
		
		//4. Check number of customers
		currentNumOfCustomers = bank.getNumberOfCustomers();
		
		//5. Assert
		assertEquals(bank.getCustomers().size(),currentNumOfCustomers);
		
		//6. Add customer(s) to bank
		bank.addCustomer("Diego", 500); //number of customer(s) increased by 1
		bank.addCustomer("Deivid", 300); //number of customer(s) increased by 1
		
		//7. Check number of customers
		currentNumOfCustomers = bank.getNumberOfCustomers();
		
		//8. Assert
		assertEquals(bank.getCustomers().size(),currentNumOfCustomers);
	}
	
	/**
	 * B3:  The bank can remove customers.  When a customer is removed, the number of Bank customers decreases by 1
	 */
	@Test 
	public void testRemoveCustFromBank(){
	    //1. Create a bank
		Bank bank = new Bank();
		
		//2. initialize current number of customers
		int currentNumOfCustomers = 0;
		
		//3 Add customer(s) to bank
		bank.addCustomer("Sherwayne", 1000); //number of customer(s) increased by 1
		bank.addCustomer("Diego", 500); //number of customer(s) increased by 1
		bank.addCustomer("Deivid", 300); //number of customer(s) increased by 1
		
		//4. Check number of customers
		currentNumOfCustomers = bank.getNumberOfCustomers();

		//5. Remove customer from bank
		bank.removeCustomer("Sherwayne");  //number of customers decrease by 1
		 
		//6. Check number of customers
	    currentNumOfCustomers = bank.getNumberOfCustomers();
	
	    //7. Assert
		 assertEquals(bank.getCustomers().size(),currentNumOfCustomers);
	
	    //8. Remove customer(s) from bank
		 bank.removeCustomer("Diego");  //number of customers decrease by 1
		 bank.removeCustomer("Deivid"); //number of customers decrease by 1	
	
	    //9. Get number of customers
		 currentNumOfCustomers = bank.getNumberOfCustomers();
	
	   //10. Assert
		 assertEquals(bank.getCustomers().size(),currentNumOfCustomers);
	}
	
	/** 
	 * B4:The bank can transfer money between two accounts
	 * B4:The bank identifies accounts based on the Customer name associated with the account.
	 * B4:When a transfer is made, the amount is deducted from Customer 1�s account, and deposited into Customer 2�s account. 
	 * B4:Customer 1 and 2�s balances should update automatically.
	 */
	@Test
	public void testBankTransfer() {
		//1. Create bank
		Bank bank = new Bank();
		
		//2. Create customer 1
		Customer customer1 = new Customer("Sherwayne",1000);
		
		//3. Create customer 2
		Customer customer2 = new Customer("Diego",500);
		
		//4. Create transfer
		bank.transferMoney(customer1, customer2, 500);
		
		//5. Get customer 1 account
		Account cust1Account = customer1.getAccount();
		
		//6. Get customer 2 account
		Account cust2Account = customer2.getAccount();
		
		//7. Get customer 1 balance
		int cust1Balance = cust1Account.balance();
		
		//8. Get customer 2 balance
		int cust2Balance = cust2Account.balance();
		
		//9. Assert customer 1 balance
		assertEquals(500,cust1Balance);
		
		//10. Assert customer 2 balance
		assertEquals(1000,cust2Balance);
	}
	
	
	/**
 	* B4: The bank identifies accounts based on the Customer name associated with the account.
 	*/
	@Test
	public void testBankTransferIdentifyCustomer1() {
		//1. Create bank
		Bank bank = new Bank();
		
		//2. Create customer 1
		Customer customer1 = new Customer("Sherwayne",1000);
		
		//3. Create customer 2
		Customer customer2 = new Customer("Diego",500);
		
		//4. Assert
		assertEquals("Sherwayne",customer1.getName());
		assertEquals("Diego",customer2.getName());
	}
	
	
	/**
 	* B4: The bank identifies accounts based on the Customer name associated with the account.
 	*/
	@Test
	public void testBankTransferIdentifyCustomer2() {
		//1. Create bank
		Bank bank = new Bank();
		
		//2. Create customer 1
		Customer customer1 = new Customer("Sherwayne",1000);
		
		//3. Create customer 2
		Customer customer2 = new Customer("Diego",500);
		
		//4. Assert
		assertEquals("Diego",customer1.getName());
	}

}
