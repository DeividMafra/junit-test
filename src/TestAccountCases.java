import static org.junit.Assert.*;

import org.junit.Test;

public class TestAccountCases {

	@Test
	public void accountOpen() {
		
		Account account = new Account(100);
		
		int balance = account.balance();
		
		assertEquals(0, balance);
		
	}
	
	@Test
	public void accountOpen1() {
		
		Account account = new Account(0);
		
		int balance = account.balance();
		
		assertEquals(0, balance);
		
	}
	
	@Test
	public void accountOpen2() {
		
		Account account = new Account(-100);
		
		int balance = account.balance();
		
		assertEquals(0, balance);
		
	}
	
	@Test
	public void accountDeposit() {
		
		Account account = new Account(100);
		
		account.deposit(100);
		
		int balance = account.balance();
		
		assertEquals(200, balance);
	}
	
	@Test
	public void accountDeposit2() {
		
		Account account = new Account(100);
		
		account.deposit(-300);
		
		int balance = account.balance();
		
		assertEquals(100, balance);
	}
	
	@Test
	public void accountDeposit3() {
		
		Account account = new Account(2099999999);
		
		account.deposit(999999999);
		
		int balance = account.balance();
		//3099999998
//		double expec = 3099999998d;
		assertTrue(balance > 2099999999);
//		assertEquals(3099999998, balance);
	}
	
	@Test
	public void accountWithdraw() {
		Account account = new Account(100);
		
		account.withdraw(50);
		
		int balance = account.balance();
		
		assertEquals(50, balance);
	}
	
	@Test
	public void accountWithdraw2() {
		Account account = new Account(100);
		
		account.withdraw(110);
		
		int balance = account.balance();
		
		assertEquals(100, balance);
	}
	
	@Test
	public void accountWithdraw3() {
		Account account = new Account(0);
		
		account.withdraw(110);
		
		int balance = account.balance();
		
		assertEquals(0, balance);
	}
	
	@Test
	public void accountWithdraw4() {
		Account account = new Account(300);
		
		account.withdraw(-110);
		
		int balance = account.balance();
		
		assertEquals(300, balance);
	}

}
