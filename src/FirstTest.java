import static org.junit.Assert.*;
import org.junit.Test;

public class FirstTest {
	
	@Test
	public void testInit () {
//		check that the balance of a newly created account is set to 0
		
		Customer c = new Customer("newCustomer", 0);
		int deposit = c.getAccount().balance();
		assertEquals(0, deposit);	
	}

//	@Test
	public void testOverdraft () {
//		make sure that an attempt to withdraw more money than the account 
//		contains doesn't change the account's balance
		
		Customer c = new Customer("newCustomer", 100);
		
		c.getAccount().withdraw(200);
		int balance = c.getAccount().balance();
	
		assertEquals(100, balance);	
	}
	
//	@Test
	public void testDeposit() {
//		make sure that the account balance reflects the result of 
//		a legal call to deposit
		
		Customer c = new Customer("newCustomer", 100);
		
		c.getAccount().deposit(200);
		int balance = c.getAccount().balance();
	
		assertEquals(300, balance);
	}
	
//	@Test
	public void testWithdraw() {
//		make sure that an attempt to withdraw more money than the account 
//		contains doesn't change the account's balance
		
		Customer c = new Customer("newCustomer", 100);
		
		c.getAccount().withdraw(50);
		int balance = c.getAccount().balance();
	
		assertEquals(50, balance);	
	}
	
//	@Test
	public void testInvalidArgs() {
//		make sure that an attempt to withdraw more money than the account 
//		contains doesn't change the account's balance
		
		Customer c = new Customer("newCustomer", 100);
		
		c.getAccount().withdraw(-50);
		int balanceW = c.getAccount().balance();
	
		assertEquals(100, balanceW);
		
		c.getAccount().deposit(-200);
		int balanceD = c.getAccount().balance();
	
		assertEquals(100, balanceD);
	}
}
